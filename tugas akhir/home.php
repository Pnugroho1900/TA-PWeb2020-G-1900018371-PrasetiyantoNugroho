<!DOCTYPE html>
<html>
<head>
	<title>POST TEST 6 & 7</title>
	<style type="text/css">
*{
	margin: 0;
	padding: 0;
}
body {
	font-family: sans-serif;
	background-image: url('1.png');
}
.menu-bar{
	background: rgb(244, 164, 96);
	text-align: center;
}
.menu-bar ul{
	display: inline-flex;
	list-style: none;
	color: #fff;
}
.menu-bar ul li{
    width: 300px;
    margin: 0px;
    padding: 3px;
}
.menu-bar ul li a{
	text-decoration: none;
	color: black;
}
.active, .menu-bar ul li:hover{
	background: #f7c9a1;
	border-radius: 3px;
}
.bawah {
	background-color: #F4A460;
	position: absolute;
	left: 0;
	bottom: 0;
	height: 25px;
	width: 100%;
	text-align: center;	
}
.sosmed{
	position: absolute;
	left: 0;
	bottom: 50px;
	height: 25px;
	width: 100%;
	text-align: center;
}
.data{
	width: 900px;
	background:#F4A460;
	border: 1px solid black;
	margin: 0px auto;
	padding: 50px 20px;
}
.hit{
	 position: absolute;
    left: 0;
    bottom: 320px;
    height: 0px;
    width: 100%;
}
	</style>
</head>
<body>
	<div class="menu-bar">
		<ul>
			<li class="active"><a href="home.php">BERANDA</a></li>
			<li><a href="pesan.php">PEMESANAN</a></li>
			<li><a href="about.php">ABOUT</a></li>
			<li><a href="buka.php">MEMBER</a></li>
			<li><a href="login.php" onclick="return confirm('Apakah Anda Yakin Ingin Keluar ?')">LOG OUT</a></li>
		</ul>
	</div>
	<div class="bawah">
		<p><font color="black">Copy Right @Pnugroho1900</font></p>
	</div>
	<div class="sosmed">
		<a href="https://www.instagram.com/prasetiyanto_"><img src="igg.png" width="35px" height="35px"></a>
		<a href="mailto:nugrohopraseyoyanto@gmail.com"><img src="gm.png" width="35px" height="30px"></a>
		<a href="https://www.facebook.com/prasnug19/"><img src="fb.png" width="35px" height="35px"></a>
		<a href="https://twitter.com/PrasetiyantoN?s=09"><img src="ttw.png" width="35px" height="35px"></a>
	</div>
	<br><br><br><br><br><br>
	<form class="data">
		<?php
	function date_indo($tanggal){ 
		$bulan = array (
			1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		$pecahkan = explode('-', $tanggal); 
 
		return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
	}

	echo "<center><h3>";
	echo "Tanggal : ".date_indo(date('Y-m-d')); 
	echo "<br/>";
	date_default_timezone_set('Asia/Jakarta');
	echo "Waktu  : ".date('H:i:s');
	echo "</center>";
?>
	</form>
	<div class="hit">
		<?php
		//buka     file    counter    mode  baca
		$filecounter="home.txt";
		$fl=fopen($filecounter,"r+");

		//ambil    nilai    hit    dan   simpan    dalam    variabel      $hit
		$hit=fread($fl,filesize($filecounter));

		//tampilkan
		echo "<br><br><br><br><br><br><br><br><br><br><br><br><br><br>";
		echo("<table       width=250    align=right       border=l    cellspacing=0 cellpadding=0    color=#F4A460><tr>");
		echo(  " <td   width=250     valign=middle     align=center>     ");
		echo(  " <font     face=verdana     size=2   color=white><b>     ");
		echo("Anda pengunjung halaman ke-");
		echo($hit);
		echo("</b></font>");
		echo("</td>");
		echo(  " </tr></table>");
		//tutup     file      counter.txt
		fclose($fl);

		//buka    file    counter.txt       mode   tulis
		$fl=fopen($filecounter,         "w+");

		//tambahkan      nilai    hit    dengan    1
		$hit=$hit+1; 

		//simpan
		fwrite($fl,$hit,strlen($hit));

		//tutup
		fclose($fl);
		?>
	</div>
</body>
</html>